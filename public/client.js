// client-side js
// run by the browser each time your view template referencing it is loaded
//la fonction pour drag et drop d'un message 
//$ = document.querySelector.bind(document);



/*Fonction qui fait verification des mots de passe*/

function deuxPassOk() {

  var password1 = document.forms["signIn"]["passwordConf"].value;
  var password2 = document.forms["signIn"]["password"].value;
  
  if(password1 != password2) {
    alert("Les deux mots de passe ne sont pas identiques");
    return false;
	}
}

	document.addEventListener('dblclick', createNote);
	function createNote(e) {

    var stickerEl = document.createElement('div'),
    form = document.createElement('form'),
    textareaEl = document.createElement('textarea'),
    coordx = document.createElement('input'),
   
    coordy= document.createElement('input'),
    butSubmit= document.createElement('input');
    stickerEl.classList.add('class'); 
    stickerEl.classList.add('id'); 
    stickerEl.setAttribute('id', 'divPopup');
    stickerEl.classList.add('style'); 
    stickerEl.setAttribute('class', 'sticker');
   // textareaEl.classList.add('name'); 
      
	 textareaEl.classList.add('id'); 
     textareaEl.classList.add('class'); 
     textareaEl.classList.add('placeholder'); 
     textareaEl.setAttribute('id', 'text');
     textareaEl.setAttribute('class', 'txt');
     textareaEl.setAttribute('name', 'textMess');
    // textareaEl.setAttribute('class', 'text');
     textareaEl.setAttribute('placeholder', 'Entrez ici votre note ');
     var  posX = e.clientX ,
        posY = e.clientY ;
  

     coordx.classList.add("type");
     coordx.setAttribute('type', 'hidden');
     coordy.classList.add("type");
     coordy.setAttribute('type', 'hidden');
     coordx.setAttribute('name', 'coordX');
     coordy.setAttribute('name', 'coordY');
     coordx.classList.add('value');
     coordy.classList.add('value');
     coordx.setAttribute('value', posX);
     coordy.setAttribute('value', posY);
    
    form.classList.add("action");
    form.classList.add("onsubmit");
    form.classList.add("method");
    form.classList.add("class");
    form.setAttribute('class', 'bar');
     
	 //form.setAttribute("onsubmit", "return valider();");
    form.setAttribute('action', '/ajouter');
    form.setAttribute('method', 'post');
    butSubmit.classList.add('value');
    butSubmit.classList.add('type');
    butSubmit.setAttribute('value', 'Enregistrer');
    butSubmit.setAttribute('type','submit');
    stickerEl.appendChild(form);
    form.appendChild(textareaEl);
    form.appendChild(butSubmit);
    form.appendChild(coordx);
    form.appendChild(coordy); 
  
    butSubmit.style.width=30;
     
    document.body.appendChild(stickerEl);
   }

	var Posts=[]; //liste des messages d'un utilisateur
	
function listeMessage() {
  
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '/liste');
    xhr.responseType = 'json';
    xhr.onload = function() {
      if(xhr.response) {
      Posts = xhr.response;
      }
    };
    xhr.setRequestHeader("Content-Type", 'application/json')
    return
}


function clicModifier(clee){
 if(document.getElementById("text"+clee).disabled != true){
  if(confirm("Voulez-vous modifier votre post ?") == true) {
   
      var xhrConnect = new XMLHttpRequest();
      xhrConnect.open('POST', '/modifier');
      var now=new Date();
      var txt=document.getElementById("text"+clee).value;
          var NoteModifee = {
			cle : clee,
			texte : txt,
			date : now,
			heure : now.getHours()
      }
			xhrConnect.responseType = "json";
			xhrConnect.setRequestHeader('Content-type', 'application/json');
			xhrConnect.send(JSON.stringify(NoteModifee));
			xhrConnect.onload = function () {
     
		if(xhrConnect.status == 200) {
         alert("Votre post est bien modifie !");
         document.getElementById("idForm"+clee).setAttribute('action', '/effacer');
         document.getElementById("text"+clee).style="background-color: #FAD7A0;";
         document.getElementById("text"+clee).disabled = true;
        
             //var d=new Date({{item['date']}});
                 var now = new Date();
                //var nn= now.toLocaleString();
                 var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
                 var nn2=now.toLocaleDateString('fr-FR', options);  
                 
                 function checkTime(i) {
                  if (i < 10) {
                    i = "0" + i;
                  }
                  return i;
                }
                
                  var h = now.getHours();
                  var m = now.getMinutes();

                  // add a zero in front of numbers<10
                  m = checkTime(m);
                  var elem = document.querySelector('#idDate'+clee);
                 elem.innerHTML = "&nbsp;"+nn2 +", "+h + ":" + m ;
        //document.getElementById("").style.display = "none";
      }
      else if(xhrConnect.status == 500) {
        alert("Erreur modification !");
      }
    };
    
			}}else{
			
			alert("Après vos changement veuillez appuyer sur le button une deuxième fois pour valider !");
 
			document.getElementById("text"+clee).disabled = false;
			document.getElementById("text"+clee).style="background-color: white;";
			document.getElementById("text"+clee).focus();
			document.getElementById("idForm"+clee).setAttribute('action', '/modifier');
}
}

 function clicSupprimer(cle) {

		if(confirm("Voulez-vous supprimer votre post ?") == true) {
    
		document.getElementById("idForm"+cle).setAttribute('action', '/effacer');
        
		var xhr = new XMLHttpRequest();
		xhr.open('post', '/effacer');
          var cleAsupp = {
		  cle : cle,
		}
          xhr.responseType = "json";
          xhr.setRequestHeader('Content-type', 'application/json');
          xhr.send(JSON.stringify(cleAsupp));
          xhr.onload = function () {
        if( xhr.status == 200) {
        var elem = document.querySelector('#divpostN'+cle);
        elem.remove();
		}
		else if(xhr.status == 500) {
        alert("Echec de la suppression !");
		}
			};
		}
		 else{
			return false;
				
}};



