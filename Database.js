var knex = require('knex')({
    client: 'sqlite3',
    connection: {
        filename: ".data/db.sqlite3"
    },
    useNullAsDefault: true,
    debug: true,
});


async function init() {
  try{
    
    knex.schema.dropTableIfExists('users')
    
    await knex.schema.createTable('users', (table) => {
    table.string('login').primary();
    table.string('password').notNullable();
    table.string('name');
  });   
      knex.schema.dropTableIfExists('messages')
    
      await knex.schema.createTable('messages', function (table) {
      table.increments('cle');
      table.string('texte').notNullable();
      table.string('auteur').notNullable().references('users.login');
      table.integer('coordX');
      table.integer('coordY');
      table.date('date').notNullable();
      table.integer('heure').unsigned().notNullable(); 
    });
  //  await knex.raw('delete * FROM messages WHERE login = :user', {});
    var rows = await knex.raw('select * FROM messages');
    //knex.schema.dropTable('messages')
  }
  catch(err){ 
   console.error('Error init_db : ', err); }

  
  var rows = await knex('messages');
  console.log('----------------\nRows:', rows);

  
  await knex.destroy();
  }

init()