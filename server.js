// server.js
// where your node app starts

// initialisation de project
var express = require('express');
var bodyParser = require('body-parser');
var session = require('express-session');
var cookieP = require('cookie-parser');
var nunjucks = require('nunjucks');
var app = express();
var knex = require('knex')({
    client: 'sqlite3',
    connection: {
        filename: ".data/db.sqlite3"
    },
    useNullAsDefault: true,
    debug: true,
});

//Configuration

app .use(bodyParser.urlencoded({ extended: true }))
    .use(cookieP())
    .use('/public', express.static('public'))
    .use(express.json());

nunjucks.configure('views', {
    express: app,
    noCache: true
});

app.use(session({
  secret : 'postit159',
  resave: false,
  saveUninitialized: false,
   cookie : {maxAge : 5 * 60 *1000}
}));


/* La route de l'index */
app.get('/', async function(req, res) {
   
		// Si l'utilisateur est connecté, on l'envoie sur l'index qui sera rempli avec ses posts
		   
		if (req.session.login && (req.session.login != "null")) {
			 var messages = await knex('messages').where('auteur', req.session.login);
			  var utilisateur = await knex('users').where('login', req.session.login);
		   
			res.render("index.html", {login : req.session.login,nom:`${utilisateur[0].name}`, messages : messages});
		}
		   // ou cas ou l'utilisateur n'est pas connecté on l'envoie sur la page principale 
		else
		   {
			   res.render("index.html", {login : req.session.login});
		   }
});

app.get('/login',function(req,res){
	
	  if (req.session.login && (req.session.login != "null")) {

		   res.render("index.html",  {login:req.session.login,error : "Utilisateur existe deja "});
	  }
	  else {
		// res.render("login.html");
		 res.render("/",  {error : "Errur Login"});
	  }
});

app.post('/login',async function(req,res){
	  try {
		  
		var users = await knex('users').where({'login' : req.body.login});
		
		if ((users.length == 1) && users[0].password==req.body.password) {                  
		  req.session.login = req.body.login;
		  res.redirect("/" );
		}
		else {
	 
		  res.render('index.html', { 
		  error: 'login ou mot de passe incorrect',
		
		});
		}
	  } 
	  catch (err) {                       
		console.error('Erreur base de donnes:', err);     
		res.send(err);
	  }
  });


app.get('/logout', (req, res) => {  
    if (req.session.login) {
      req.session.login = "null";
    }
    res.redirect("/");
});

app.get('/signin', (req, res) => {
	 // res.render('signin.html');
	if (req.session.login && (req.session.login != "null")) {
		res.redirect("/" + req.session.login);
	  }
	else {
		res.render("signin.html", {});
	  }
});


app.post('/signin', async (req, res) => {
 
  try {
    
    if(req.body.name && req.body.login && req.body.password ) {
     
    var row = await knex('users')
                    .where('login', req.body.login);
      
    //Row contient l' utilisatuer recherche
    if(!row[0]) {
      await knex.raw('INSERT INTO users VALUES (?, ?, ?)',
                    [req.body.login, req.body.password,req.body.name]);
        
      req.session.login = req.body.login;
      res.redirect("/" );
    }
      else{
      res.render("signin.html", {error : "Utilisateur existe déjà"});
    }
    
    } else {
       res.render("signin.html", {error : "Erreur Formulaire"});
    }}
      
  
  catch(e){
    res.send(e);
  }
});



/*test userliste pour recuperer tous les utilisateurs existant dans la bd*/
app.get('/userlist', async (req, res) => {
       try {
       res.render('userlist.html', { 
        users: await knex('users'),
      current: req.session.login,
      });
    } catch (err) {
      console.error(err);
      res.status(500).send('Error');
    }
});

/* ---------------- Les evenements----------------*/
var router = express.Router()
app.route('/effacer')
  .post(async (req, res) =>{
 
  
    console.log("post received\n------------");
   console.dir(req.body);
 // res.redirect('/listMess');
  try {
        await knex('messages')
        .where({
            'cle' : req.body.cle,
        })
        .del();
    console.log("bien supprime");
    res.status(200);
    res.send(JSON.stringify(req.body.cle));
     
  // res.send("Delete success");
     //res.render('index.html',{login: req.session.login}); 
  
  }
  catch(e) {
    res.status(500);
    res.send("Suppression echouee");
  }
  });

app.route('/modifier').post(async  (req, res) =>{

		console.log("post received\n------------");
		console.dir(req.body);
		 // res.redirect('/listMess');
			try { 
			console.log("post recieved");
		  
			//   res.send(req.query);
		  
			// res.send("Delete success");
			
			//res.render('index.html',{login: req.session.login});
			var now = new Date();
			await   knex('messages')
		  .where('cle', req.body.cle)
		  .update({
			texte: req.body.texte,
			date: now,
			  heure:req.body.heure
		  })
			   console.log("bien modifiee");
			  res.status(200);
			  res.send("success");
			
		   
		  }
		  catch(e) {
			res.status(500);
			res.send("modification echouee");
		  }
});

app.post('/ajouter', async (req, res) => {

  try { 
    if (req.session.login && (req.session.login != "null")) {
    if(req.body.textMess!=""){
    var now = new Date();
    var data = {
          auteur :       req.session.login,
          texte :        req.body.textMess,
          date :         now,
          heure :        now.getHours(),//selon le fuseau horaire et l'heure d'ete
          coordX :       req.body.coordX,
          coordY :       req.body.coordY 
     };
            await knex('messages').insert(data);
  
    res.status(200);
   // res.send(id);
    // res.send("   req.body.auteur "+req.body.auteur+"   req.body.texte, "+req.body.textMess);
     res.redirect('/'); 
         
  }else{
    res.status(400);//bad request
    
  }}else {
   res.send("Erreur login");
  }
  }
  catch(e) {
    res.status(500);
    console.log(e);
    res.send("Erreur insertion");
  }
});

app.get('/liste', async  (req, res) =>{
  
		  try {
			var messages = await knex('messages').where('auteur', req.session.login);
		  }
		  catch(e) {
			console.log(e);
			res.send(e);
		  }
		  res.status(200);
		  res.send(JSON.stringify(messages));
});
// listen for requests :)

var listener = app.listen(4500, function() {
  console.log('Your app is listening on port ' + listener.address().port);
});