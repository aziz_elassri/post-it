# Projet Post It

Le projet concerne la creation des messages. Chaque utilisateur connecté peut creer ses propres notes et les enregistré avec date de creation.

Il est  possible de consulter les notes sous la structure suivantes

* **signin** : pour enregistrer utilisateur
* **server** : pour lancer l'app cote server
* **views** : les fichiers statiques
* **Database.js** : initialisation de la base de données

une représentation sous forme de powerpoint sera ridégé pour détailler le fonctionnement de l'application.
